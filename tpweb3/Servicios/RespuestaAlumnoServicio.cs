﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Security.Policy;
using System.Web;
using System.Web.Configuration;
using tpweb3.Dao;
using tpweb3.Models;

namespace tpweb3.Servicios
{
    public class RespuestaAlumnoServicio
    {
        public static List<RespuestaAlumno> filtrarRespuestas(int? filtro, int id) {
            // 0 sin corregir, 1 correctas, 2 regular, 3 mal
            List <RespuestaAlumno> respuestas = null;
            if (filtro == 0)
            {
                respuestas = RespuestaAlumnoDao.filtrarPorSinCorregir(id);
            }
            else if (filtro == 1)
            {
                respuestas = RespuestaAlumnoDao.filtrarPorCorrectas(id);
            }
            else if (filtro == 2)
            {
               respuestas = RespuestaAlumnoDao.filtrarPorRegular(id);
            }
            else if (filtro == 3)
            {
                respuestas = RespuestaAlumnoDao.filtrarPorMal(id);
            }
            else
            {
                respuestas = RespuestaAlumnoDao.traerRespuestasPorIdPregunta(id);
            }
            return respuestas;
            }

        public static bool verificarSiHayRespuestasSinCalificar(int id)
        {
            List<RespuestaAlumno> respuestas = RespuestaAlumnoDao.filtrarPorSinCorregir(id);
            bool isEmpty = !respuestas.Any();
            if (isEmpty)
            {
                return false;
            }
            else {
                return true;
            }
        }
        public static bool verificarSiHayMejorRespuesta(int id) {
            RespuestaAlumno respuesta = RespuestaAlumnoDao.traerConMejorRespuesta(id);
            if (respuesta == null)
            {
                return false;
            }
            else {
                return true;
            }
        }

        public static bool enviarEmailMejorRespuesta(int id, string dominio) {
            RespuestaAlumno helper = RespuestaAlumnoDao.traerPorId(id);
            Pregunta pregunta = PreguntaDao.traerPreguntaPorId(helper.IdPregunta);
            Alumno alumno = AlumnoDao.traerAlumnoPorId(helper.IdAlumno);
            String posicionesLink = dominio + "/Alumno/Inicio";
            String respuestaLink = dominio + $"/Alumno/VerRespuesta/{id}";
            using (MailMessage emailMessage = new MailMessage())
            {
                emailMessage.From = new MailAddress("rocioaylenamilla@gmail.com", "Programación web 3");
                emailMessage.To.Add(new MailAddress("rocioaylenamilla@gmail.com", $"{alumno.Nombre} {alumno.Apellido}"));
                emailMessage.Subject = "Su respuesta ha sido marcada como la mejor. ¡Felicitaciones!";
                emailMessage.Body = $"Su respuesta ha sido marcada como la mejor. Pregunta: { pregunta.Pregunta1}. Respuesta:" + respuestaLink + ".Posiciones: "+ posicionesLink + ". ¡Felicitaciones!";
                emailMessage.IsBodyHtml = true;
                emailMessage.Priority = MailPriority.Normal;
                using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                {
                    MailClient.EnableSsl = true;
                    MailClient.Credentials = new System.Net.NetworkCredential("paramandaremails@gmail.com", "Prueba123");
                    MailClient.Send(emailMessage);
                }
            }
          return true;
        }

        public static bool enviarEmailCorregido(int id, int calificacion, string dominio)
        {
            RespuestaAlumno helper = RespuestaAlumnoDao.traerPorId(id);
            Pregunta pregunta = PreguntaDao.traerPreguntaPorId(helper.IdPregunta);
            Alumno alumno = AlumnoDao.traerAlumnoPorId(helper.IdAlumno);
            String calificacionString = ResultadoEvaluacionDao.traerNombreResultado(calificacion);
            String posicionesLink = dominio + "/Alumno/Inicio";
            String respuestasLink = dominio + $"/Alumno/VerRespuesta/{id}";
            //alumno.Email para probar puse el mio
            using (MailMessage emailMessage = new MailMessage())
            {
                emailMessage.From = new MailAddress("paramandaremails@gmail.com", "Programación web 3");
                emailMessage.To.Add(new MailAddress($"{alumno.Email}", $"{alumno.Nombre} {alumno.Apellido}"));
                emailMessage.Subject = $"Su respuesta ha sido calificada como {calificacionString}";
                emailMessage.Body = $"Pregunta: { pregunta.Pregunta1}. Respuesta: {respuestasLink}.Posiciones: {posicionesLink}.";
                emailMessage.Priority = MailPriority.Normal;
                using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                {
                    MailClient.EnableSsl = true;
                    MailClient.Credentials = new System.Net.NetworkCredential("paramandaremails@gmail.com", "Prueba123");
                    MailClient.Send(emailMessage);
                }
            }
            return true;
        }
        public static long calcularPuntaje(int respCorrectas, int calificacion) {
            int puntajeMax = Int32.Parse(WebConfigurationManager.AppSettings["PuntajeMaximoPorRespuestaCorrecta"]);
            int cupo = Int32.Parse(WebConfigurationManager.AppSettings["CupoMaximoRespuestasCorrectas"]);
            if (calificacion == 1)
            {
                if (puntajeMax - (puntajeMax / cupo * (respCorrectas)) <= 0)
                {
                    return puntajeMax / cupo;
                }
                else
                {
                    return puntajeMax - (puntajeMax / cupo * (respCorrectas));
                }
            }
            else if (calificacion == 2)
            {
                return (puntajeMax - (puntajeMax / cupo * (respCorrectas))) / 2;
            }
            else {
                return 0;
            }
        }

        public static bool enviarEmailPreguntaRespondida(RespuestaAlumno respuesta, string dominio)
        {
            List<Profesor> profesores = ProfesorDao.traerProfesores();
            Alumno alumno = AlumnoDao.traerAlumnoPorId(respuesta.IdAlumno);
            Pregunta pregunta = PreguntaDao.traerPreguntaPorId(respuesta.IdPregunta);
            //falta colocar link de la respuseta
            String posicionesLink = dominio + $"/Profesor/Evaluar/{respuesta.IdPregunta}";
            using (MailMessage emailMessage = new MailMessage())
            {
                emailMessage.From = new MailAddress("paramandaremails@gmail.com", "Programación web 3");
                /*foreach(Profesor profesor in profesores) { 
                emailMessage.To.Add(new MailAddress($"{profesor.Email}", $"{profesor.Nombre} {profesor.Apellido}"));
                }*/
                emailMessage.To.Add(new MailAddress($"rocioaylenamilla@gmail.com", $"Prueba"));

                emailMessage.Subject = $"Respuesta a pregunta {pregunta.Nro} - {respuesta.Orden} - {alumno.Apellido}";
                emailMessage.Body = $"Pregunta: {pregunta.Pregunta1}. Alumno: {alumno.Nombre} - {alumno.Apellido}. Orden: {respuesta.Orden}. Respuesta: {respuesta.Respuesta}. Evaluar: {posicionesLink}";
                emailMessage.Priority = MailPriority.Normal;
                using (SmtpClient MailClient = new SmtpClient("smtp.gmail.com", 587))
                {
                    MailClient.EnableSsl = true;
                    MailClient.Credentials = new System.Net.NetworkCredential("paramandaremails@gmail.com", "Prueba123");
                    MailClient.Send(emailMessage);
                }
            }
            return true;
        }
    }
    }

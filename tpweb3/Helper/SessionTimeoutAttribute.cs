﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace tpweb3.Helper
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext ctx = HttpContext.Current;
            if (HttpContext.Current.Session["ID"] == null)
            {
                var values = new RouteValueDictionary(new
                {
                    action = "Login",
                    controller = "Login",
                    exceptiontext = HttpContext.Current.Request.Url.AbsoluteUri
            });
                filterContext.Result = new RedirectToRouteResult(values);
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}

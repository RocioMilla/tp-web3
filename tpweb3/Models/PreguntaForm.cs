﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tpweb3.Models
{
    public class PreguntaForm
    {
            [Required]
            public int idProfesor { get; set; }
            [Required(ErrorMessage = "El número es requerido")]
            public int nro { get; set; }
            [Required(ErrorMessage = "La clase es requerida")]
            public int clase { get; set; }
            [Required(ErrorMessage = "El tema es requerido")]
            public int tema { get; set; }
            public DateTime? disponibleDesde { get; set; }
            public DateTime? disponibleHasta { get; set; }
            [Required(ErrorMessage = "La pregunta es requerida")]
            public string pregunta { get; set; }
        }
    }
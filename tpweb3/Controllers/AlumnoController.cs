﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tpweb3.Dao;
using tpweb3.Helper;
using tpweb3.Models;
using tpweb3.Servicios;

namespace tpweb3.Controllers
{
    [SessionTimeout]
    [Alumno]
    public class AlumnoController : Controller
    {
        // GET: Alumno
        [HttpGet]
        public ActionResult Inicio()
        {

            Alumno alumno = new Alumno();
            alumno = AlumnoDao.mostrarAlumnoPorDni(int.Parse(Session["id"].ToString()));

            List<Alumno> listaAlumnos = new List<Alumno>();
            List<Pregunta> preguntas = new List<Pregunta>();
            Clase clase = new Clase();
            List<RespuestaAlumno> respuesta = new List<RespuestaAlumno>();
            List<Pregunta> SinResponder = new List<Pregunta>();

            listaAlumnos = AlumnoDao.mostrarTodosLosAlumnos();
            preguntas = PreguntaDao.preguntasUltimaClase();
            clase = ClaseDao.mostrarUltimaClase();
            SinResponder = PreguntaDao.mostrarPreguntasSinResponder(int.Parse(Session["id"].ToString()));

            ViewBag.SinResponder = SinResponder;
            ViewBag.preguntas = preguntas;
            ViewBag.clase = clase;
            ViewBag.alumno = alumno;
            ViewBag.listaAlumnos = listaAlumnos;
            return View();

        }

        [ActionName("acerca-de")]
        public ActionResult acercaDe()
        {
            return View();
        }


        public ActionResult Preguntas(String filtro)
        {
            if (filtro != "Sin Corregir" && filtro != "Correcta" && filtro != "Regular" && filtro != "Mal")
            {
                filtro = "Todas";
            }
            Alumno alumno = new Alumno();
            alumno = AlumnoDao.mostrarAlumnoPorDni(int.Parse(Session["id"].ToString()));
            List<Pregunta> preguntas = PreguntaDao.filtrar(alumno.IdAlumno, filtro);

            ViewBag.preguntas = preguntas;
            ViewBag.alumno = alumno;
            ViewBag.filtro = filtro;

            return View();

        }


        [HttpGet]
        public ActionResult Responder(int id)
        {
            Alumno alumno = new Alumno();
            alumno = AlumnoDao.mostrarAlumnoPorDni(int.Parse(Session["id"].ToString()));
            Pregunta pregunta = PreguntaDao.mostrarPreguntaPorId(id);
            ViewBag.alumno = alumno;

            Session["idPregunta"] = pregunta.IdPregunta;
            RespuestaAlumno respuesta = new RespuestaAlumno();
            respuesta.Pregunta = pregunta;
            return View(respuesta);
        }

        [HttpPost]
        public ActionResult CrearRespuesta(RespuestaAlumnoForm r)
        {
            if (ModelState.IsValid)
            {
                RespuestaAlumno respuesta = new RespuestaAlumno();
                respuesta = RespuestaAlumnoDao.CrearRespuestaAlumno(r, int.Parse(Session["id"].ToString()), int.Parse(Session["idPregunta"].ToString()));
                Uri myuri = new Uri(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                string pathQuery = myuri.PathAndQuery;
                string hostName = myuri.ToString().Replace(pathQuery, "");
                RespuestaAlumnoServicio.enviarEmailPreguntaRespondida(respuesta, hostName);
                //respuesta.Pregunta = PreguntaDao.mostrarPreguntaSinResponder(int.Parse(Session["idPregunta"].ToString()));
                return RedirectToAction("Preguntas", "Alumno");
            }
            else {
                TempData["respuesta"] = "Hubo errores al subir la respuesta";
                return Redirect("/Profesor/Inicio");
            }
        }

        [HttpGet]
        public ActionResult VerRespuesta(int id)
        {
            Alumno alumno = new Alumno();
            alumno = AlumnoDao.mostrarAlumnoPorDni(int.Parse(Session["id"].ToString()));
            RespuestaAlumno respuesta = new RespuestaAlumno();
            ViewBag.alumno = alumno;
            respuesta = RespuestaAlumnoDao.BuscarRespuestaPorId(id, alumno.IdAlumno);
            return View(respuesta);

        }
    }
}
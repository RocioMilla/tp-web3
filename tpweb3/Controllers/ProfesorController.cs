﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tpweb3.Dao;
using tpweb3.Helper;
using tpweb3.Models;
using tpweb3.Servicios;

namespace tpweb3.Controllers
{
    [SessionTimeout]
    [Profesor]
    public class ProfesorController : Controller
    {
            // GET: Profesor
            public ActionResult Inicio()
            {
                    ViewBag.preguntas = PreguntaDao.traerPreguntas();
                    return View();
            }
        [ActionName("acerca-de")]
        public ActionResult acercaDe()
        {
            return View();
        }

        public ActionResult CrearPregunta()
            {
                ViewBag.ultimoNro = PreguntaDao.ObtenerUltimoNumeroPregunta();
                ViewBag.clases = ClaseDao.traerClases();
                ViewBag.temas = TemaDao.traerTemas();
                return View();
            }

            [HttpPost]
            public ActionResult altaPregunta(PreguntaForm param)
            {
            if (ModelState.IsValid)
            {
                if (PreguntaDao.traerPreguntaPorNro(param.nro) != null)
                {
                    TempData["respuesta"] = "Número de pregunta ya existe";
                    return Redirect("/Profesor/error");
                }
                else
                {
                    PreguntaDao.crearPregunta(param);
                    return Redirect("/Profesor/inicio");
                }
            }
            else {
                TempData["respuesta"] = "Hubo errores al crear la pregunta";
                return Redirect("/Profesor/error");
            }
            }

        public ActionResult Modificar(int id)
        {
            if (PreguntaDao.verificarSiPreguntaTieneRespuestas(id))
            {
                ViewBag.hayRespuestas = true;
            }
            else {
                ViewBag.hayRespuestas = null;
            }
            ViewBag.pregunta = PreguntaDao.traerPreguntaPorId(id);
            ViewBag.clases = ClaseDao.traerClases();
            ViewBag.temas = TemaDao.traerTemas();
            return View();
        }

        [HttpPost]
        public ActionResult editarPregunta(int id, PreguntaForm param) {
            if (ModelState.IsValid)
            {
                if (PreguntaDao.editarPregunta(id, param))
                {
                    return Redirect("/Profesor/Inicio");
                }
                else
                {
                    TempData["respuesta"] = "Hubo errores al editar";
                    TempData["class"] = "danger";
                    return Redirect("/Profesor/Inicio");
                }
            }
            else {
                TempData["respuesta"] = "Hubo errores al editar";
                TempData["class"] = "danger";
                return Redirect("/Profesor/Inicio");
            }
        }
        public ActionResult Eliminar(int id)
        {
            if (PreguntaDao.verificarSiPreguntaTieneRespuestas(id))
            {
                TempData["respuesta"] = "No se puede eliminar pregunta con respuestas";
                return Redirect("/profesor/error");
            }
            else
            {
                if (PreguntaDao.eliminarPregunta(id))
                {
                    return Redirect("/Profesor/Inicio");
                }
                else
                {
                    TempData["respuesta"] = "Hubo un error al eliminar la pregunta";
                    return Redirect("/Profesor/error");
                }
            }
        }
        public ActionResult error()
        {
            return View();
        }

        public ActionResult Evaluar(int id, int? filtro)
        {
            ViewBag.respuestas = RespuestaAlumnoServicio.filtrarRespuestas(filtro, id);
            ViewBag.haySinCalificar = RespuestaAlumnoServicio.verificarSiHayRespuestasSinCalificar(id);
            ViewBag.hayMejorRespuesta = RespuestaAlumnoServicio.verificarSiHayMejorRespuesta(id);
            ViewBag.pregunta = PreguntaDao.traerPreguntaPorId(id);
            return View();
        }

        [HttpPost]
        public ActionResult Corregir(int idRespuestaAlumno, int idProfesor, int calificacion) {
            if (!RespuestaAlumnoDao.Corregir(idRespuestaAlumno, idProfesor, calificacion))
            {
                TempData["respuesta"] = "Hubo un error al corregir, intente más tarde.";
                return Redirect("/profesor/error");
            }
            else
            {
                Uri myuri = new Uri(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                string pathQuery = myuri.PathAndQuery;
                string hostName = myuri.ToString().Replace(pathQuery, "");
                RespuestaAlumnoServicio.enviarEmailCorregido(idRespuestaAlumno, calificacion, hostName);
                var param = RespuestaAlumnoDao.traerPorId(idRespuestaAlumno);
                return Redirect($"/profesor/evaluar/{param.IdPregunta}");
            }
        }

        public ActionResult marcarMejorRespuesta(int id)
        {
            if (!RespuestaAlumnoDao.marcarMejorRespuesta(id))
            {
                TempData["respuesta"] = "Hubo un error al corregir, intente más tarde.";
                return Redirect("/profesor/error");
            }
            else
            {
                Uri myuri = new Uri(System.Web.HttpContext.Current.Request.Url.AbsoluteUri);
                string pathQuery = myuri.PathAndQuery;
                string hostName = myuri.ToString().Replace(pathQuery, "");
                RespuestaAlumnoServicio.enviarEmailMejorRespuesta(id, hostName);
                var param = RespuestaAlumnoDao.traerPorId(id);
                return Redirect($"/profesor/evaluar/{param.IdPregunta}");
            }
        }
    }
}
 
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using tpweb3.Dao;
using tpweb3.Models;

namespace tpweb3.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Login(string exceptionText)
        {
            if (Session["rol"] == null)
            {
                ViewBag.url = exceptionText;
                return View();
            }
            else {
                var rol = Session["rol"].ToString();
                if (rol == "1")
                {
                    return RedirectToAction("Inicio", "Profesor");
                }
                else {
                    return RedirectToAction("Inicio", "Alumno");
                }
            }

        }

        [HttpPost]
        public ActionResult validarLogin(LoginForm param, String urlRedirect)
        {
            if(ModelState.IsValid){
                if ("True" == param.soyProfesor.ToString())
                {
                    Profesor profesor = ProfesorDao.buscarProfesor(param.email, param.password);
                    if (profesor == null || profesor.Password != param.password)
                    {
                        TempData["datosInvalidos"] = true;
                        return Redirect("/");
                    }
                    Session["id"] = profesor.IdProfesor;
                    Session["rol"] = 1;
                    if (urlRedirect != null)
                    {
                        return Redirect(urlRedirect);
                    }
                    else
                    {
                        return RedirectToAction("Inicio", "Profesor");
                    }
                }
                else
                {
                    Alumno alumno = AlumnoDao.mostrarAlAlumno(param.email, param.password);
                    if (alumno == null || alumno.Password != param.password)
                    {
                        TempData["datosInvalidos"] = true;
                        return Redirect("/");
                    }
                    Session["id"] = alumno.IdAlumno;
                    Session["rol"] = 0;
                    if (urlRedirect != null)
                    {
                        return Redirect(urlRedirect);
                    }
                    else
                    {
                        return RedirectToAction("Inicio", "Alumno");
                    }
                }
            }else{
                return Redirect("/");
            }
        }

        public ActionResult Salir() {
            Session.Abandon();
            return Redirect("/Login/Login");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tpweb3.Dao
{
    public static class TemaDao
    {
        public static List<Tema> traerTemas()
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Tema.ToList();
        }
    }
}

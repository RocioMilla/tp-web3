﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Globalization;
using tpweb3.Models;
using System.Data.Entity;


namespace tpweb3.Dao
{
    public static class PreguntaDao
    {
        public static List<Pregunta> traerPreguntas()
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Pregunta.OrderByDescending(pregunta => pregunta.Nro).ToList();
        }


        public static List<Pregunta> filtrar(int id, String filtro)
        {
            if (filtro == "Todas")
            {
                return traerPreguntas();
            }
            if (filtro == "Sin Corregir")
            {
                return mostrarPreguntasSinEvaluar(id);
            }
            if(filtro == "Mal")
            {
                return mostrarTodasLasPreguntasRespondidasMal(id);
            }
            if(filtro == "Regular")
            {
                return mostrarTodasLasPreguntasRespondidasRegular(id);
            }

            return mostrarTodasLasPreguntasRespondidasCorrecta(id);
            
        }

        public static List<Pregunta> mostrarPreguntasSinEvaluar(int id)
        {
            List<Pregunta> preguntas = new List<Pregunta>();

            TP_20191CEntities context = new TP_20191CEntities();

            var pre = from p in context.Pregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      join t in context.Tema on p.Tema.IdTema equals t.IdTema
                      join r in context.RespuestaAlumno on p.IdPregunta equals r.Pregunta.IdPregunta
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      join a in context.Alumno on r.Alumno.IdAlumno equals a.IdAlumno
                      where a.IdAlumno == id
                      select p;


            var pre2 = from p in context.Pregunta
                       join c in context.Clase on p.Clase.IdClase equals c.IdClase
                       join t in context.Tema on p.Tema.IdTema equals t.IdTema
                       where pre.All(pr => pr.IdPregunta != p.IdPregunta)
                       orderby p.Nro descending
                       select p;

            preguntas = pre2.ToList();


            return preguntas;
        }


        public static List<Pregunta> mostrarTodasLasPreguntasRespondidasMal(int id)
        {
            List<Pregunta> preguntas = new List<Pregunta>();
            TP_20191CEntities context = new TP_20191CEntities();
            var pre = from p in context.Pregunta
                  join c in context.Clase on p.Clase.IdClase equals c.IdClase
                  join t in context.Tema on p.Tema.IdTema equals t.IdTema
                  join r in context.RespuestaAlumno on p.IdPregunta equals r.Pregunta.IdPregunta
                  join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                  where r.Alumno.IdAlumno == id && re.Resultado == "Mal" && r.MejorRespuesta== false
                  orderby p.Nro descending
                  select p;

            preguntas = pre.ToList();

            return preguntas;
        }
        public static List<Pregunta> mostrarTodasLasPreguntasRespondidasRegular(int id)
        {
            List<Pregunta> preguntas = new List<Pregunta>();
            TP_20191CEntities context = new TP_20191CEntities();
            var pre = from p in context.Pregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      join t in context.Tema on p.Tema.IdTema equals t.IdTema
                      join r in context.RespuestaAlumno on p.IdPregunta equals r.Pregunta.IdPregunta
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where r.Alumno.IdAlumno == id && re.Resultado == "Regular" && r.MejorRespuesta==false
                      orderby p.Nro descending
                      select p;

            preguntas = pre.ToList();

            return preguntas;
        }

        public static List<Pregunta> mostrarTodasLasPreguntasRespondidasCorrecta(int id)
        {
            List<Pregunta> preguntas = new List<Pregunta>();
            TP_20191CEntities context = new TP_20191CEntities();
            var pre = from p in context.Pregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      join t in context.Tema on p.Tema.IdTema equals t.IdTema
                      join r in context.RespuestaAlumno on p.IdPregunta equals r.Pregunta.IdPregunta
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where r.Alumno.IdAlumno == id && re.Resultado == "Correcta"
                      orderby p.Nro descending
                      select p;

            preguntas = pre.ToList();

            return preguntas;
        }

        public static List<Pregunta> mostrarPreguntasSinResponder(int id)
        {
            List<Pregunta> preguntas = new List<Pregunta>();

            TP_20191CEntities context = new TP_20191CEntities();
            DateTime fechaHoy = DateTime.Now;

            return context.Pregunta.Where(p => p.FechaDisponibleDesde <= fechaHoy && p.FechaDisponibleHasta >= fechaHoy && !p.RespuestaAlumno.Any(r => r.IdAlumno == id)).ToList();
        }



        public static List<Pregunta> mostrarPreguntasSinCorregir(int id)
        {
            List<Pregunta> preguntas = new List<Pregunta>();

            TP_20191CEntities context = new TP_20191CEntities();
            preguntas = context.Pregunta.Include(p => p.RespuestaAlumno).Where(p => p.RespuestaAlumno.Any(r => r.FechaHoraEvaluacion == null && r.IdAlumno == id)).ToList();
            return preguntas;
        }
        public static void crearPregunta(PreguntaForm param)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            Pregunta pregunta = new Pregunta();
            pregunta.Nro = param.nro;
            pregunta.IdClase = param.clase;
            pregunta.IdTema = param.tema;
            pregunta.FechaDisponibleDesde = param.disponibleDesde;
            pregunta.FechaDisponibleHasta = param.disponibleHasta;
            pregunta.Pregunta1 = param.pregunta;
            pregunta.IdProfesorCreacion = param.idProfesor;
            pregunta.FechaHoraCreacion = DateTime.Now;
            ctx.Pregunta.Add(pregunta);
            ctx.SaveChanges();
        }

        public static int ObtenerUltimoNumeroPregunta()
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Pregunta.Max(pregunta => pregunta.Nro);
        }

        public static Pregunta traerPreguntaPorNro(int nro)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Pregunta.Where(pregunta => pregunta.Nro == nro).FirstOrDefault();
        }
        public static Pregunta traerPreguntaPorId(int nro)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Pregunta.Where(pregunta => pregunta.IdPregunta == nro).FirstOrDefault();
        }

        public static bool editarPregunta(int id, PreguntaForm param)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            Pregunta pregunta = ctx.Pregunta.Find(id);
            pregunta.Nro = param.nro;
            pregunta.IdClase = param.clase;
            pregunta.IdTema = param.tema;
            pregunta.Pregunta1 = param.pregunta;
            pregunta.FechaHoraModificacion = DateTime.Now;
            pregunta.FechaDisponibleDesde = param.disponibleDesde;
            pregunta.FechaDisponibleHasta = param.disponibleHasta;
            pregunta.IdProfesorModificacion = param.idProfesor;
            ctx.SaveChanges();
            return true;

        }

        public static bool verificarSiPreguntaTieneRespuestas(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            List<RespuestaAlumno> param = ctx.RespuestaAlumno.Where(r => r.IdPregunta == id).ToList();
            if (param.Count() != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool eliminarPregunta(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            Pregunta pregunta = ctx.Pregunta.Find(id);
            ctx.Pregunta.Remove(pregunta);
            ctx.SaveChanges();
            return true;
        }

        public static List<Pregunta> preguntasUltimaClase()
        {
            Clase clase = new Clase();
            List<Pregunta> preguntas = new List<Pregunta>();
            TP_20191CEntities context = new TP_20191CEntities();
            clase = ClaseDao.mostrarUltimaClase();

            var pre = (from p in context.Pregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      where c.IdClase == clase.IdClase
                      orderby p.Nro descending
                      select p).Take(2);

            preguntas = pre.ToList();
            return preguntas;
        }

        public static Pregunta mostrarPreguntaPorId(int idPregunta)
        {
            Pregunta pregunta = new Pregunta();
            TP_20191CEntities context = new TP_20191CEntities();
            var pre = from p in context.Pregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      join t in context.Tema on p.Tema.IdTema equals t.IdTema
                      where p.IdPregunta == idPregunta
                      select p;

            pregunta = pre.FirstOrDefault();
            return pregunta;
        }



    }
}
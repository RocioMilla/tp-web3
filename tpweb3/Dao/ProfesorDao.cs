﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace tpweb3.Dao
{
    public static class ProfesorDao
    {
        public static Profesor buscarProfesor(string email, string password)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Profesor.Where(profesor => profesor.Email == email && profesor.Password == password).FirstOrDefault();
        }

        public static Profesor buscarProfesorPorId(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Profesor.Where(profesor => profesor.IdProfesor == id).FirstOrDefault();
        }

        public static List<Profesor> traerProfesores() {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Profesor.ToList();
        }
    }
}

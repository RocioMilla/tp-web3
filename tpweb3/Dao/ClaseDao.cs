﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace tpweb3.Dao
{
    public static class ClaseDao
    {
        public static List<Clase> traerClases()
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.Clase.ToList();
        }

        public static Clase mostrarUltimaClase()
        {
            Clase clase = new Clase();
            TP_20191CEntities context = new TP_20191CEntities();
            DateTime hoy = DateTime.Now;

            var cla = from c in context.Clase
                      join p in context.Pregunta on c.IdClase equals p.Clase.IdClase
                      join r in context.RespuestaAlumno on p.IdPregunta equals r.Pregunta.IdPregunta
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where c.Fecha <= hoy
                      orderby c.Fecha descending
                      select c;

            /*var cla = from c in context.Clase
                      where c.IdClase == 1
                      select c;*/

            clase = cla.FirstOrDefault();
            return clase;
        }
    }
}

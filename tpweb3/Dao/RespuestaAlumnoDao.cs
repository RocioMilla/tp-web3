﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using tpweb3.Models;
using tpweb3.Servicios;

namespace tpweb3.Dao
{
    public static class RespuestaAlumnoDao
    {
        public static List<RespuestaAlumno> Respuestas(int idPregunta)
        {
            List<RespuestaAlumno> lista = new List<RespuestaAlumno>();
            TP_20191CEntities context = new TP_20191CEntities();
            var res = from r in context.RespuestaAlumno
                      join p in context.Pregunta on r.Pregunta.IdPregunta equals p.IdPregunta
                      join a in context.Alumno on r.Alumno.IdAlumno equals a.IdAlumno
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where p.IdPregunta == idPregunta
                      select r;

            lista = res.ToList();
           

            return lista;
        }

        public static RespuestaAlumno mostrarResultado(int idPregunta, int idAlumno)
        {
            RespuestaAlumno respuesta = new RespuestaAlumno();
            TP_20191CEntities context = new TP_20191CEntities();
            var pre = from r in context.RespuestaAlumno
                      //join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where r.Pregunta.IdPregunta == idPregunta && r.Alumno.IdAlumno == idAlumno
                      select r;

            respuesta = pre.FirstOrDefault();

            return respuesta;
        }



        public static List<RespuestaAlumno> traerRespuestasPorIdPregunta(int id) {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.IdPregunta == id).OrderBy(f => f.FechaHoraRespuesta).ToList();
        }

        public static List<RespuestaAlumno> filtrarPorSinCorregir(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.IdPregunta == id && rptaAlumno.IdProfesorEvaluador == null).OrderBy(f => f.FechaHoraRespuesta).ToList();
        }
        public static List<RespuestaAlumno> filtrarPorCorrectas(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.IdPregunta == id && rptaAlumno.IdResultadoEvaluacion == 1).OrderBy(f => f.FechaHoraRespuesta).ToList();
        }
        public static List<RespuestaAlumno> filtrarPorRegular(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.IdPregunta == id && rptaAlumno.IdResultadoEvaluacion == 2).OrderBy(f => f.FechaHoraRespuesta).ToList();
        }
        public static List<RespuestaAlumno> filtrarPorMal(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.IdPregunta == id && rptaAlumno.IdResultadoEvaluacion == 3).OrderBy(f => f.FechaHoraRespuesta).ToList();
        }

        public static RespuestaAlumno traerConMejorRespuesta(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.MejorRespuesta == true && rptaAlumno.IdPregunta == id).FirstOrDefault();
        }
        public static bool Corregir(int id, int profesor, int calificacion)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            RespuestaAlumno respuesta = ctx.RespuestaAlumno.Find(id);
            respuesta.IdResultadoEvaluacion = calificacion;
            respuesta.IdProfesorEvaluador = profesor;
            respuesta.FechaHoraEvaluacion = DateTime.Now;
            Pregunta pregunta = PreguntaDao.traerPreguntaPorId(respuesta.IdPregunta);
            int correctasMomento = obtenerCorrectasHastaElMomento(pregunta.IdPregunta);
            if (calificacion == 1)
            {
                respuesta.RespuestasCorrectasHastaElMomento = correctasMomento + 1;
                respuesta.Alumno.CantidadRespuestasCorrectas += 1;
            }
            else
            {
               respuesta.RespuestasCorrectasHastaElMomento = correctasMomento;
            }
            var param = RespuestaAlumnoServicio.calcularPuntaje(correctasMomento, calificacion);
            respuesta.Puntos = param;
            respuesta.Alumno.PuntosTotales = param + respuesta.Alumno.PuntosTotales;
            ctx.SaveChanges();
            return true;
        }
        
        public static int obtenerCorrectasHastaElMomento(int id) {
            TP_20191CEntities ctx = new TP_20191CEntities();
            int? param = ctx.RespuestaAlumno.Where(r => r.IdPregunta == id).Max(r => r.RespuestasCorrectasHastaElMomento);
            if (param == null) {
                return 0;
                } else {
                return (int)param;
            }

        }
        public static RespuestaAlumno traerPorId(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            return ctx.RespuestaAlumno.Where(rptaAlumno => rptaAlumno.IdRespuestaAlumno == id).FirstOrDefault();
        }
        public static bool marcarMejorRespuesta(int id)
        {
            TP_20191CEntities ctx = new TP_20191CEntities();
            RespuestaAlumno respuesta = ctx.RespuestaAlumno.Find(id);
            respuesta.MejorRespuesta = true;
            respuesta.Puntos = respuesta.Puntos + (Int32.Parse(WebConfigurationManager.AppSettings["PuntajeMaximoPorRespuestaCorrecta"]) / 2);
            respuesta.Alumno.PuntosTotales = (long)respuesta.Puntos + respuesta.Alumno.PuntosTotales;
            respuesta.Alumno.CantidadMejorRespuesta += 1;
            ctx.SaveChanges();
            return true;
        }
        public static RespuestaAlumno CrearRespuestaAlumno(RespuestaAlumnoForm r, int idAlumno, int idPregunta)
        {
            TP_20191CEntities context = new TP_20191CEntities();
            Alumno alumno = new Alumno();
            RespuestaAlumno respuesta = new RespuestaAlumno();
            alumno = AlumnoDao.mostrarAlumnoPorDni(idAlumno);
            Pregunta pregunta = PreguntaDao.mostrarPreguntaPorId(idPregunta);

            respuesta.Respuesta = r.respuesta;
            respuesta.FechaHoraRespuesta = DateTime.Now;
            respuesta.IdAlumno = alumno.IdAlumno;
            respuesta.IdPregunta = pregunta.IdPregunta;
            respuesta.Orden = RespuestaAlumnoDao.obtenerUltimoOrden(pregunta.IdPregunta) + 1;

            context.RespuestaAlumno.Add(respuesta);
            context.SaveChanges();
            return respuesta;

        }

        public static int obtenerUltimoOrden(int id) {
            TP_20191CEntities ctx = new TP_20191CEntities();
            RespuestaAlumno param = ctx.RespuestaAlumno.Where(r => r.IdPregunta == id).FirstOrDefault();
            if (param == null)
            {
                return 0;
            }
            else
            {
                return ctx.RespuestaAlumno.Where(r => r.IdPregunta == id).Max(r => r.Orden);
            }
        }
        public static RespuestaAlumno BuscarRespuestaPorId(int idPregunta, int idAlumno)
        {
            TP_20191CEntities context = new TP_20191CEntities();
            RespuestaAlumno respuesta = new RespuestaAlumno();

            var res = from r in context.RespuestaAlumno
                      join a in context.Alumno on r.Alumno.IdAlumno equals a.IdAlumno
                      join p in context.Pregunta on r.Pregunta.IdPregunta equals p.IdPregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      join t in context.Tema on p.Tema.IdTema equals t.IdTema
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where p.IdPregunta == idPregunta && a.IdAlumno == idAlumno
                      select r;

            respuesta = res.FirstOrDefault();
            if (respuesta == null)
            {
                var res2 = from r in context.RespuestaAlumno
                           join a in context.Alumno on r.Alumno.IdAlumno equals a.IdAlumno
                           join p in context.Pregunta on r.Pregunta.IdPregunta equals p.IdPregunta
                           join c in context.Clase on p.Clase.IdClase equals c.IdClase
                           join t in context.Tema on p.Tema.IdTema equals t.IdTema
                           where p.IdPregunta == idPregunta && a.IdAlumno == idAlumno
                           select r;

                respuesta = res2.FirstOrDefault();
            }
            return respuesta;
        }

        public static List<RespuestaAlumno> mostrarRespuestasUltimaClase(int idpregunta)
        {
            TP_20191CEntities context = new TP_20191CEntities();
            List<RespuestaAlumno> respuestas = new List<RespuestaAlumno>();
            var res = from r in context.RespuestaAlumno
                      join p in context.Pregunta on r.Pregunta.IdPregunta equals p.IdPregunta
                      join c in context.Clase on p.Clase.IdClase equals c.IdClase
                      join a in context.Alumno on r.Alumno.IdAlumno equals a.IdAlumno
                      join re in context.ResultadoEvaluacion on r.ResultadoEvaluacion.IdResultadoEvaluacion equals re.IdResultadoEvaluacion
                      where p.IdPregunta == idpregunta
                      orderby r.Puntos descending
                      select r;

            respuestas = res.ToList();
            return respuestas;
        }

    }
}